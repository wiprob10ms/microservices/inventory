package b10.rafaelbraga.implementation.inventory.api;


import b10.rafaelbraga.implementation.inventory.dto.ResponseDto;
import b10.rafaelbraga.implementation.inventory.entity.Inventory;
import b10.rafaelbraga.implementation.inventory.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("inventory")
public class InventoryController {

    @Autowired
    public InventoryController(InventoryService service) {
        this.service = service;
    }

    private InventoryService service;

    @PostMapping(produces = "application/json")
    public ResponseEntity<ResponseDto> addStock(@RequestBody @Valid Inventory inventory) {
        Inventory newInventory = service.createInventory(inventory);
        return new ResponseEntity<>(newInventory.entityCreated(), HttpStatus.CREATED);
    }

    @PatchMapping(path = "/consume", produces = "application/json")
    public ResponseEntity<ResponseDto> reduce(@Valid @RequestBody Inventory inventory) {
        Inventory reducedInventory = service.reduceStock(inventory);
        return ResponseEntity.ok(reducedInventory.reducedStock(inventory.getStockQuantity()));
    }

    @PatchMapping(path = "/increment", produces = "application/json")
    public ResponseEntity<ResponseDto> add(@Valid @RequestBody Inventory inventory) {
        Inventory addedInventory = service.addStock(inventory);
        return ResponseEntity.ok(addedInventory.incrementedStock(inventory.getStockQuantity()));
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<Inventory>> getInventories() {
        return ResponseEntity.ok(service.getAll());
    }

    @GetMapping(path = "/{productCode}", produces = "application/json")
    public ResponseEntity<Inventory> getInventory(@PathVariable String productCode) {
        return ResponseEntity.ok(service.ofProduct(productCode));
    }



}
