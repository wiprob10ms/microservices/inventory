package b10.rafaelbraga.implementation.inventory.service;

import b10.rafaelbraga.implementation.inventory.entity.Inventory;

import java.util.List;

public interface InventoryService {

    Inventory createInventory(Inventory inventory);
    Inventory reduceStock(Inventory inventory);
    Inventory addStock(Inventory inventory);
    void removeProductFrom(Inventory inventory);
    List<Inventory> getAll();
    Inventory ofProduct(String code);

}
