package b10.rafaelbraga.implementation.inventory.service.impl;

import b10.rafaelbraga.implementation.inventory.entity.Inventory;
import b10.rafaelbraga.implementation.inventory.exception.DuplicatedProductException;
import b10.rafaelbraga.implementation.inventory.exception.ProductNotFoundException;
import b10.rafaelbraga.implementation.inventory.repository.InventoryRepository;
import b10.rafaelbraga.implementation.inventory.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("inventoryService")
public class InventoryServiceImpl implements InventoryService {

    private InventoryRepository repository;

    @Autowired
    public InventoryServiceImpl(InventoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public Inventory createInventory(Inventory inventory) {
        Optional<Inventory> product = repository.findByProductCode(inventory.getProductCode());
        product.ifPresent(s -> {
            throw new DuplicatedProductException(inventory.getProductCode());
        });
        return repository.save(inventory);
    }

    @Override
    public Inventory addStock(Inventory inventory) {
        return repository.save(getInventory(inventory).addStock(inventory.getStockQuantity()));
    }

    @Override
    public Inventory reduceStock(Inventory inventory) {
        return repository.save(getInventory(inventory).reduceStock(inventory.getStockQuantity()));
    }

    @Override
    public void removeProductFrom(Inventory inventory) {
        repository.deleteById(inventory.getProductCode());
    }

    @Override
    public List<Inventory> getAll() {
        return repository.findAll();
    }

    @Override
    public Inventory ofProduct(String code) {
        return repository.findByProductCode(code).orElseThrow(() -> new ProductNotFoundException(code));
    }

    private Inventory getInventory(Inventory inventory) {
        Optional<Inventory> product = repository.findByProductCode(inventory.getProductCode());
        return product.orElseThrow(() -> new ProductNotFoundException(inventory.getProductCode()));
    }

}
