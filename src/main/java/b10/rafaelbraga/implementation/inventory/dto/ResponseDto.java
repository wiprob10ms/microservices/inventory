package b10.rafaelbraga.implementation.inventory.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
public class ResponseDto {

    private int status;
    private String message;
    private BigDecimal stockQuantity;

}
