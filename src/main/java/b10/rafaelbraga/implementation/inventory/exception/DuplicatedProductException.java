package b10.rafaelbraga.implementation.inventory.exception;

public class DuplicatedProductException extends RuntimeException {

    private static final String MESSAGE = "A product with %s already exists on the database";

    public DuplicatedProductException(String productCode) {
        super(String.format(MESSAGE, productCode));
    }

    public DuplicatedProductException(String productCode, Throwable cause) {
        super(String.format(MESSAGE, productCode), cause);
    }

}
