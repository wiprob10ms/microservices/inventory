package b10.rafaelbraga.implementation.inventory.exception;

public class InvalidStockQuantityException extends RuntimeException {

    private static final String MESSAGE = "Quantity for product %s cannot be negative";

    public InvalidStockQuantityException(String productCode) {
        super(String.format(MESSAGE, productCode));
    }

    public InvalidStockQuantityException(String productCode, Throwable cause) {
        super(String.format(MESSAGE, productCode), cause);
    }

}
