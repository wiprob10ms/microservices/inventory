package b10.rafaelbraga.implementation.inventory.exception;

public class ProductNotFoundException extends RuntimeException {

    private static final String MESSAGE = "Product %s wasn't found on the database";

    public ProductNotFoundException(String productCode) {
        super(String.format(MESSAGE, productCode));
    }

    public ProductNotFoundException(String productCode, Throwable cause) {
        super(String.format(MESSAGE, productCode), cause);
    }

}
