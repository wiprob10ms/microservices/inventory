package b10.rafaelbraga.implementation.inventory.repository;

import b10.rafaelbraga.implementation.inventory.entity.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InventoryRepository extends JpaRepository<Inventory, String> {

    Inventory save(Inventory account);
    void deleteAll();
    void deleteById(String id);
    Optional<Inventory> findByProductCode(String productCode);
    List<Inventory> findAll();

}
