package b10.rafaelbraga.implementation.inventory.entity;

import b10.rafaelbraga.implementation.inventory.dto.ResponseDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Inventory {

    public static final String INSUFFICIENT_STOCK_MESSAGE = "Product %s doesn't have %s quantity to consume";
    public static final String THE_STOCK_QUANTITY_SHOULD_BE_GREATER_THAN_0 = "The stock quantity should be greater than 0";
    @Id
    @Column(unique = true)
    @NotNull(message = "productCode is mandatory")
    private String productCode;
    @NotNull(message = "stockQuantity is mandatory")
    private BigDecimal stockQuantity;

    private static final String CREATE_MESSAGE = "Stock for product %s created";
    private static final String CONSUMED_MESSAGE = "Stock for product %s diminished by %s";
    private static final String INCREMENTED_MESSAGE = "Stock for product %s incremented by %s";

    public ResponseDto entityCreated() {
        return ResponseDto.builder()
                .status(HttpStatus.CREATED.value())
                .message(String.format(CREATE_MESSAGE, this.productCode))
                .stockQuantity(this.getStockQuantity())
                .build();
    }

    public ResponseDto reducedStock(BigDecimal decrementedQuantity) {
        return ResponseDto.builder()
                .status(HttpStatus.OK.value())
                .message(String.format(CONSUMED_MESSAGE, this.productCode, decrementedQuantity))
                .stockQuantity(this.getStockQuantity())
                .build();
    }

    public ResponseDto incrementedStock(BigDecimal incrementedQuantity) {
        return ResponseDto.builder()
                .status(HttpStatus.OK.value())
                .message(String.format(INCREMENTED_MESSAGE, this.productCode, incrementedQuantity))
                .stockQuantity(this.getStockQuantity())
                .build();
    }

    public Inventory reduceStock(BigDecimal stock) {
        if (this.getStockQuantity().compareTo(stock) >= 0) {
            return new Inventory(this.getProductCode(), this.getStockQuantity().subtract(stock));
        }
            throw new IllegalArgumentException(String.format(INSUFFICIENT_STOCK_MESSAGE, this.productCode, stock));
    }

    public Inventory addStock(BigDecimal stock) {
        if (stock.compareTo(BigDecimal.ZERO) > 0) {
            return new Inventory(this.getProductCode(), this.getStockQuantity().add(stock));
        }
        throw new IllegalArgumentException(THE_STOCK_QUANTITY_SHOULD_BE_GREATER_THAN_0);
    }



}
