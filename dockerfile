# Alpine because it's lighter
FROM openjdk:8-jdk-alpine
MAINTAINER Rafael Braga <rafael.braga@wipro.com>

# Set ENV variables
ENV PORT=8089

# Add JAR file and run it as entrypoint
ADD target/inventory-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]

# Expose the port
EXPOSE 8089